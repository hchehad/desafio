var imageIndex = 0;
var imagesList = $('.image-container div');


function updateSlide() {
  var item = $('.image-container div').eq(imageIndex);
  imagesList.hide();
  item.css('display','inline-block');
}

$('.next').click(function() {
  if (imageIndex == imagesList.length-1) return;
  imageIndex += 1;
  updateSlide();
});


$('.prev').click(function() {
  if (imageIndex == 0) return;
  imageIndex -= 1;
  updateSlide();
});


$(function() {
  updateSlide();
});