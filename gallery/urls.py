# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from django.conf import settings


urlpatterns = patterns(
    'gallery.views',

    url(r'^$', 'home', name='home'),
    url(r'^list/?$', 'list', name='list'),
    url(r'^manage/?$', 'manage', name='manage'),
    url(r'^manage/(?P<image_pk>\d+)/?$', 'manage', name='manage'),
    url(r'^delete/(?P<image_pk>\d+)/?$', 'delete', name='delete'),

)

