# -*- coding: utf-8 -*-

from django.shortcuts import render, get_object_or_404
from .models import Image
from .forms import ImageForm
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

def home(request):
    context = {}

    image_list = Image.objects.all().order_by('-id')

    context['list'] = image_list
    context['total'] = image_list.count()

    return render(request, 'home.html', context)


def manage(request, image_pk=None):
    context = {}
    instance = None

    # Check if is edit mode

    if image_pk:
        instance = get_object_or_404(Image, pk=image_pk)

    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES, instance=instance)
        if form.is_valid():
            form.save()
            context['success'] = True

    else:
        form = ImageForm(instance=instance)

    context['form'] = form

    if instance:
        context['title'] = u'Editar Imagem'
    else:
        context['title'] = u'Adicionar Imagem'

    return render(request, 'manage.html', context)


def list(request):
    context = {}

    image_list = Image.objects.all().order_by('-id')

    context['list'] = image_list
    return render(request, 'list.html', context)



def delete(request, image_pk=None):
    context = {}

    instance = get_object_or_404(Image, pk=image_pk)
    instance.delete()
    return HttpResponseRedirect(reverse('list'))
