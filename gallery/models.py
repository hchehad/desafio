# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings


class Image(models.Model):
    title = models.CharField(u'Título', max_length=80)
    image = models.FileField(u'Imagem')

    def __unicode__(self):
        return u'%s' % (self.title)

    def get_url(self):
        return u'%s%s' % (settings.MEDIA_URL, self.image)